#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "wordsbylenght.h"

using namespace testing;

TEST(NumOfWordsByLenghtCase, NoResultIfTextIsEmpty)
{
  // arrange
  std::string text {""};
  std::map<size_t, size_t> wordsCount;
  size_t numOfLetters = 1;

  // act
  countWordsByLenght(text, wordsCount);

  // assert
  EXPECT_EQ(wordsCount[numOfLetters], 0);
}

TEST(NumOfWordsByLenghtCase, TwoWordsWithOneLetter)
{
  // arrange
  std::string text {"a more words b"};
  std::map<size_t, size_t> wordsCount;
  size_t numOfLetters = 1;

  // act
  countWordsByLenght(text, wordsCount);

  // assert
  EXPECT_EQ(wordsCount[numOfLetters], 2);
}

TEST(NumOfWordsByLenghtCase, TwoWordsWithTwoLetters)
{
  // arrange
  std::string text {"aa more words bb"};
  std::map<size_t, size_t> wordsCount;
  size_t numOfLetters = 2;

  // act
  countWordsByLenght(text, wordsCount);

  // assert
  EXPECT_EQ(wordsCount[numOfLetters], 2);
}

TEST(NumOfWordsByLenghtCase, ThreeWordsWithFourLetters)
{
  // arrange
  std::string text {"a more more more words b"};
  std::map<size_t, size_t> wordsCount;
  size_t numOfLetters = 4;

  // act
  countWordsByLenght(text, wordsCount);

  // assert
  EXPECT_EQ(wordsCount[numOfLetters], 3);
}
