#include "wordsbylenght.h"
#include <map>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <iostream>

void countWordsByLenght(std::string text, std::map<size_t, size_t>& numOfWordsByLenght)
{
  if(text.empty())
  {
    return;
  }

  std::vector<std::string> words;
  boost::split(words, text, boost::is_any_of("\t "));

  for(auto word : words)
  {
      numOfWordsByLenght[word.length()]++;
  }
}

