#include "removefifth.h"

void removeFifth(List *root)
{
  List *current = root;
  List *prev = root;
  int i = 0;
  while(current)
  {
    i++;
    if (i == 5)
    {
      prev->next = current->next;
      delete current;
      current = prev->next;
      i = 0;
    }
    else
    {
      prev = current;
      current = current->next;
    }
  }
}
