#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "removefifth.h"

using namespace testing;

class MyFixture : public ::testing::Test {
public:
  List *head = nullptr;

  void SetUp() {
    head = new List();
    List *current = head;
    head->payLoad.i = 0;
    for(int i = 1; i <= 12; i++)
    {
      List * newList = new List();
      newList->payLoad.i = i;
      current->next = newList;
      current = current->next;
    }
  }

  List *find(int n)
  {
    List *current = head;
    while(current)
    {
      if(current->payLoad.i == n)
      {
        return current;
      }
      current = current->next;
    }
    return nullptr;
  }

  void TearDown()
  {

    List *previous = head;
    List *current = head;
    while(current)
    {
      std::cout << current->payLoad.i << std::endl;
      previous = current;
      current = current->next;
      delete previous;
    }
    head = nullptr;
    previous = nullptr;
  }
};


TEST_F(MyFixture, RemoveFifthSet)
{
  // arrange

  // act
  removeFifth(head);

  // assert
  EXPECT_FALSE(find(4));
  EXPECT_FALSE(find(9));
  EXPECT_TRUE(find(3));
  EXPECT_TRUE(find(7));
}
