#ifndef NPRIME_H
#define NPRIME_H

#include <vector>

bool isPrime(std::size_t n);

void nPrime(std::size_t n, std::vector<std::size_t>& prime_arr);

#endif // NPRIME_H
