#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "nprime.h"

using namespace testing;

TEST(isPrimeReturnCase, ZeroAndOneAreNotPrime)
{
  // arrange
  std::size_t zero = 0;
  std::size_t one = 1;

  // act

  // assert
  ASSERT_FALSE(isPrime(zero));
  ASSERT_FALSE(isPrime(one));
}

TEST(isPrimeReturnCase, FourAndEightAreNotPrime)
{
  // arrange
  std::size_t four = 4;
  std::size_t eight = 8;
  // act

  // assert
  ASSERT_FALSE(isPrime(four) && isPrime(eight));
}


TEST(isPrimeReturnCase, threeAndFiveArePrime)
{
  // arrange
  std::size_t three = 3;
  std::size_t five = 5;

  // act

  // assert
  ASSERT_TRUE(isPrime(three));
  ASSERT_TRUE(isPrime(five));
}

TEST(nPrimeReturnArraySizeCase, ArraySizeEqualToZero)
{
  // arrange
  std::vector<size_t> primeNumbers;
  std::size_t number = 0;

  // act
  nPrime(number, primeNumbers);

  // assert
  ASSERT_EQ(primeNumbers.size(), 0);

}

TEST(nPrimeReturnArraySizeCase, ArraySizeEqualToOne)
{
  // arrange
  std::vector<size_t> primeNumbers;
  std::size_t number = 1;

  // act
  nPrime(number, primeNumbers);

  // assert
  ASSERT_EQ(primeNumbers.size(), 1);
}

TEST(nPrimeReturnArraySizeCase, ArraySizeEqualToFive)
{
  // arrange
  std::vector<size_t> primeNumbers;
  std::size_t number = 5;

  // act
  nPrime(number, primeNumbers);

  // assert
  ASSERT_EQ(primeNumbers.size(), 5);
}

TEST(nPrimeReturnArrayElementsEqualityCase, FirstElementEqualsTwo)
{
  // arrange
  std::vector<size_t> primeNumbers;
  std::size_t number = 2;

  // act
  nPrime(number, primeNumbers);

  // assert
  ASSERT_EQ(primeNumbers.at(0), 2);
}


TEST(nPrimeReturnArrayElementsEqualityCase, SecondElementEqualsThree)
{
  // arrange
  std::vector<size_t> primeNumbers;
  std::size_t number = 2;

  // act
  nPrime(number, primeNumbers);

  // assert
  ASSERT_EQ(primeNumbers.at(1), 3);
}

TEST(nPrimeReturnArrayElementsEqualityCase, ThirdAndFourthAreFiveAndSeven)
{
  // arrange
  std::vector<size_t> primeNumbers;
  std::size_t number = 4;

  // act
  nPrime(number, primeNumbers);

  // assert
  ASSERT_EQ(primeNumbers.at(2), 5);
  ASSERT_EQ(primeNumbers.at(3), 7);
}
