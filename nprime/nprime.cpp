#include "nprime.h"
#include <cmath>

bool isPrime(std::size_t n)
{
  if(n < 2)
  {
    return false;
  }
  for (std::size_t i = 2; i <= std::sqrt(n); i++)
  {
    if (n % i == 0)
    {
      return false;
    }
  }
  return true;
}

void nPrime(std::size_t n, std::vector<std::size_t>& prime_arr)
{
  int i = 0;
  while (prime_arr.size() < n)
  {
    if(isPrime(i))
    {
      prime_arr.push_back(i);
    }
    i++;
  }}

