#pragma once

#include <vector>
#include <list>

struct TreeNode
{
  TreeNode *leftChild = nullptr;
  TreeNode *rightChild = nullptr;
  int data = 0;
  TreeNode(int d)
    :data(d) {}
};

int depthTree(TreeNode *root, std::list<int> path, std::list<std::list<int>>& paths);

void remove(TreeNode *node);

void Print(std::list<std::list<int>> paths);
