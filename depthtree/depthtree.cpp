#include <iostream>
#include "depthtree.h"
#include <algorithm>


int depthTree(TreeNode *root, std::list<int> path, std::list<std::list<int>>& paths)
{

  if(root == nullptr)
  {
    return 0;
  }
  path.push_back(root->data);
  int leftDepth = depthTree(root->leftChild, path, paths);
  int rightDepth = depthTree(root->rightChild, path, paths);

  int depth = 1 + std::max(leftDepth, rightDepth);

  if(root->leftChild == nullptr && root->rightChild == nullptr)
  {
    paths.push_back(path);
    paths.sort(std::greater<std::list<int> >());
    auto maxPathSize = paths.back().size();
    paths.remove_if([maxPathSize](std::list<int>& p){ return  p.size() < maxPathSize; });
  }

  return depth;
}

void remove(TreeNode *node)
{
  if(node == nullptr)
  {
    return;
  }

  remove(node->leftChild);
  remove(node->rightChild);
  delete node;
  node = nullptr;
}

void Print(std::list<std::list<int>> paths)
{
  for(auto &path : paths)
  {
    for(auto el : path)
    {
      std::cout << el << " ";
    }
    std::cout << std::endl;
  }
}
