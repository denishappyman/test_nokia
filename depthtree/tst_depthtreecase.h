#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "depthtree.h"

using namespace testing;

class MyFixturedDepth0 : public ::testing::Test {
public:
  TreeNode *root = nullptr;
  std::list<std::list<int>> paths;
  std::list<int> path;

  void SetUp()
  {
  }

  void TearDown()
  {
    remove(root);
  }
};

class MyFixturedDepth1 : public ::testing::Test {
public:
  TreeNode *root = nullptr;
  std::list<std::list<int>> paths;
  std::list<int> path;

  void SetUp()
  {
    root = new TreeNode(0);
  }

  void TearDown()
  {
    remove(root);
  }
};

class MyFixtureBalancedDepth2 : public ::testing::Test {
public:
  TreeNode *root = nullptr;
  std::list<std::list<int>> paths;
  std::list<int> path;

  void SetUp()
  {
    // balanced tree
    root = new TreeNode(0);
    root->leftChild = new TreeNode(1);
    root->rightChild = new TreeNode(2);
  }

  void TearDown()
  {
    remove(root);
  }
};

class MyFixtureBalancedDepth3 : public ::testing::Test {
public:
  TreeNode *root = nullptr;
  std::list<std::list<int>> paths;
  std::list<int> path;

  void SetUp()
  {
    // balanced tree
    root = new TreeNode(0);

    root->leftChild = new TreeNode(1);
    root->rightChild = new TreeNode(2);

    root->leftChild->leftChild = new TreeNode(3);
    root->leftChild->rightChild = new TreeNode(4);

    root->rightChild->leftChild = new TreeNode(5);
    root->rightChild->rightChild = new TreeNode(6);
  }

  void TearDown()
  {
    remove(root);
  }
};

class MyFixtureNotBalancedDepth4 : public ::testing::Test {
public:
  TreeNode *root = nullptr;
  std::list<std::list<int>> paths;
  std::list<int> path;

  void SetUp()
  {
    // balanced tree
    root = new TreeNode(0);

    root->leftChild = new TreeNode(1);
    root->rightChild = new TreeNode(2);

    root->leftChild->leftChild = new TreeNode(3);
    root->leftChild->rightChild = new TreeNode(4);
    root->leftChild->leftChild->leftChild = new TreeNode(5);
  }

  void TearDown()
  {
    remove(root);
  }
};

TEST_F(MyFixturedDepth0, ReturnDepth0)
{
  // arrange

  // act
  int depth = depthTree(root, path, paths);
  Print(paths);

  // assert
  EXPECT_EQ(depth, 0);
}

TEST_F(MyFixturedDepth1, ReturnDepth1)
{
  // arrange

  // act
  int depth = depthTree(root, path, paths);
  Print(paths);

  // assert
  EXPECT_EQ(depth, 1);
}

TEST_F(MyFixtureBalancedDepth2, DepthMustBe2)
{
  // arrange

  // act
  int depth = depthTree(root, path, paths);
  Print(paths);

  // assert
  EXPECT_EQ(depth, 2);
}

TEST_F(MyFixtureBalancedDepth3, DepthMustBe3)
{
  // arrange

  // act
  int depth = depthTree(root, path, paths);
  Print(paths);

  // assert
  EXPECT_EQ(depth, 3);
}

TEST_F(MyFixtureNotBalancedDepth4, DepthMustBe4)
{
  // arrange

  // act
  int depth = depthTree(root, path, paths);
  Print(paths);

  // assert
  EXPECT_EQ(depth, 4);
}

TEST_F(MyFixtureBalancedDepth3, NumOfPathsMustBe4)
{
  // arrange

  // act
  int depth = depthTree(root, path, paths);
  Print(paths);

  // assert
  EXPECT_EQ(paths.size(), 4);
}

TEST_F(MyFixtureNotBalancedDepth4, NumOfPathsMustBe1)
{
  // arrange

  // act
  int depth = depthTree(root, path, paths);
  Print(paths);

  // assert
  EXPECT_EQ(paths.size(), 1);
}
