#pragma once

#include <map>

std::pair<uint32_t, uint32_t> getMinMaxUInt(uint32_t num);
