#include "getminmaxuint.h"

std::pair<uint32_t, uint32_t> getMinMaxUInt(uint32_t num)
{
  if(!num || num == UINT32_MAX)
  {
    return std::pair<uint32_t, uint32_t>(num, num);
  }

  uint32_t min = 0;
  uint32_t max = 0;
  uint32_t twoPower = 1;

  while (num >= twoPower)
  {
    if((num & twoPower) == twoPower)
    {
      min |= twoPower;
    }
    twoPower <<= 1;
  }

  max = min;

  while((max & UINT32_MAX) != UINT32_MAX)
  {
    max <<= 1;
  }

  return std::pair<uint32_t, uint32_t>(min, max);
}
