#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "getminmaxuint.h"

using namespace testing;

TEST(GetMinMaxUInt, ReturnPairWithZerosIfNumIsZero)
{
  // arrange
  uint32_t num = 0;

  // act
  auto result = getMinMaxUInt(num);

  // assert
  ASSERT_EQ(result.first, 0);
  ASSERT_EQ(result.second, 0);
}

TEST(GetMinMaxUInt, ReturnPairWithUINT32_MAXIfNumIsUINT32_MAX)
{
  // arrange
  uint32_t num = UINT32_MAX;

  // act
  auto result = getMinMaxUInt(num);

  // assert
  ASSERT_EQ(result.first, 4294967295);
  ASSERT_EQ(result.second, 4294967295);
}

TEST(GetMinMaxUInt, MinRightBitMaxLeftBit)
{
  // arrange
  uint32_t one = 1;
  uint32_t two = 2;
  uint32_t eight = 4;

  // act
  auto result1 = getMinMaxUInt(one);
  auto result2 = getMinMaxUInt(two);
  auto result8 = getMinMaxUInt(eight);

  // assert
  // first bit is set (from right side)
  ASSERT_EQ(result1.first, 1);
  // last bit is set (from left side)
  ASSERT_EQ(result1.second, 2147483648);

  ASSERT_EQ(result1, result2);
  ASSERT_EQ(result2, result8);
}

TEST(GetMinMaxUInt, Min2RightBitsMax2LeftBits)
{
  // arrange
  uint32_t three = 3;
  uint32_t five = 5;
  uint32_t six = 6;

  // act
  auto result3 = getMinMaxUInt(three);
  auto result5 = getMinMaxUInt(five);
  auto result6 = getMinMaxUInt(six);

  // assert
  // first 2 bits are set (from right side)
  ASSERT_EQ(result3.first, 3);
  // last 2 bits are set (from left side)
  ASSERT_EQ(result5.second, 3221225472);

  ASSERT_EQ(result3, result5);
  ASSERT_EQ(result5, result6);
}
